Ejecutar las pruebas del proyecto "UnitTest" -> "MainUnitTest".

Se implementaron 10 casos de pruebas, los cuales proveen la siguiente información del juego:
    * Dimenciones del tablero (new (width, height)).
    * Inicialización de células vivas (Init).
    * Definir resultado esperado para una posición especifica (i : eje x, j eje y).
    * Realizar iteración del juego (Play).
    * Obtener estado resultante del tablero (CellIsAlive).
    * Comprar resultado esperado con resultado de célula.
    
Se realizó la implementación de con forme las reglas del juego:
    * Si una célula esta viva, seguirá viva si tiene 2 o 3 células vivas como vecinas.
    * Si una célula esta viva, morirá si tiene menos de 2 células vivas como vecinas.
    * Si una célula esta viva, morirá si tiene más de 3 células vivas como vecinas.
    * Si una célula esta muerta, renacerá solo si tiene 3 células vivas como vecinas.
    * Si una célula esta muerta, seguirá muerta si tiene menos de 3 células vivas como vecinas.
    * Si una célula esta muerta, seguirá muerta si tiene más de 3 células vivas como vecinas.
    
Se utilizó una matriz de booleanos nullables, que permiten almacenar la estructura de las células.
