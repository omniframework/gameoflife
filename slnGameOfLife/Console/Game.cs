﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console
{
    public class Game
    {
        private bool?[,] Board { get; set; }
        public int Width { get; }
        public int Height { get; }

        public Game(int width, int height)
        {
            this.Width = width;
            this.Height = height;
            this.Board = new bool?[width, height];
        }

        public void Init(List<KeyValuePair<int, int>> initialState)
        {
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    this.Board[i, j] = initialState.Any(x => x.Key == i && x.Value == j);
                }
            }
        }

        public void Play()
        {
            bool?[,] TmpBoard = new bool?[Width, Height];
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    int livesCells = 0;
                    livesCells += ValueCellState(i - 1, j - 1);
                    livesCells += ValueCellState(i, j - 1);
                    livesCells += ValueCellState(i + 1, j - 1);
                    livesCells += ValueCellState(i + 1, j);
                    livesCells += ValueCellState(i + 1, j + 1);
                    livesCells += ValueCellState(i, j + 1);
                    livesCells += ValueCellState(i - 1, j + 1);
                    livesCells += ValueCellState(i - 1, j);

                    if (Board[i, j] ?? false)
                    {
                        //Esta viva y se desea comprobar si va seguir viva o morirá.
                        TmpBoard[i, j] = livesCells == 2 || livesCells == 3;
                    }
                    else
                    {
                        //Esta muerta y se desea comprobar si va a seguir muerta o va a nacer.
                        TmpBoard[i, j] = livesCells == 3;
                    }
                }
            }
            this.Board = TmpBoard;
        }

        private int ValueCellState(int i, int j)
        {
            return CellIsALive(i, j) ? 1 : 0;
        }

        public bool CellIsALive(int i, int j)
        {
            if (i >= 0 && j >= 0 && i < Width && j < Height)
            {
                return this.Board[i, j] ?? false;
            }
            return false;
        }
    }
}
