﻿using System;
using System.Collections.Generic;
using Console;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class MainUnitTest
    {
        [TestMethod]
        public void LiveCellCase2Lives()
        {            
            Game game = new Game(3, 3);

            //Inicialización de juego
            game.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(0, 0),
                new KeyValuePair<int, int>(1, 1),
                new KeyValuePair<int, int>(0, 2)
            });

            bool? expected = true;

            game.Play();

            bool? result = game.IsAlive(1, 1);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void LiveCellCase3Lives()
        {
            Game game = new Game(3, 3);

            //Inicialización de juego
            game.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(0, 0),
                new KeyValuePair<int, int>(1, 1),
                new KeyValuePair<int, int>(0, 1),
                new KeyValuePair<int, int>(0, 2)
            });

            bool? expected = true;

            game.Play();

            bool? result = game.IsAlive(1, 1);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void LiveCellCase0Lives()
        {
            Game game = new Game(3, 3);

            //Inicialización de juego
            game.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(1, 1)
            });

            bool? expected = false;

            game.Play();

            bool? result = game.IsAlive(1, 1);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void LiveCellCase1Lives()
        {
            Game game = new Game(3, 3);

            //Inicialización de juego
            game.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(0, 0),
                new KeyValuePair<int, int>(1, 1)
            });

            bool? expected = false;

            game.Play();

            bool? result = game.IsAlive(1, 1);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void LiveCellCase4Lives()
        {
            Game game = new Game(3, 3);

            //Inicialización de juego
            game.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(0, 0),
                new KeyValuePair<int, int>(0, 1),
                new KeyValuePair<int, int>(0, 2),
                new KeyValuePair<int, int>(1, 1),
                new KeyValuePair<int, int>(1, 2)
            });

            bool? expected = false;

            game.Play();

            bool? result = game.IsAlive(1, 1);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void LiveCellCase5Lives()
        {
            Game game = new Game(3, 3);

            //Inicialización de juego
            game.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(0, 0),
                new KeyValuePair<int, int>(0, 1),
                new KeyValuePair<int, int>(0, 2),
                new KeyValuePair<int, int>(1, 1),
                new KeyValuePair<int, int>(1, 1),
                new KeyValuePair<int, int>(2, 1)
            });

            bool? expected = false;

            game.Play();

            bool? result = game.IsAlive(1, 1);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DeadCellCase1Lives()
        {
            Game game = new Game(3, 3);

            //Inicialización de juego
            game.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(2, 0),
                new KeyValuePair<int, int>(2, 1),
                new KeyValuePair<int, int>(2, 2),
                new KeyValuePair<int, int>(0, 2),
                new KeyValuePair<int, int>(1, 2)                
            });

            bool? expected = false;

            game.Play();

            bool? result = game.IsAlive(0, 0);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DeadCellCase2Lives()
        {
            Game game = new Game(3, 3);

            //Inicialización de juego
            game.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(2, 0),
                new KeyValuePair<int, int>(2, 1),
                new KeyValuePair<int, int>(2, 2),
                new KeyValuePair<int, int>(0, 2),
                new KeyValuePair<int, int>(1, 2)
            });

            bool? expected = false;

            game.Play();

            bool? result = game.IsAlive(0, 1);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DeadCellCase3Lives()
        {
            Game game = new Game(3, 3);

            //Inicialización de juego
            game.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(1, 0),
                new KeyValuePair<int, int>(1, 2),
                new KeyValuePair<int, int>(2, 1)
            });

            bool? expected = true;

            game.Play();

            bool? result = game.IsAlive(1, 1);

            Assert.AreEqual(expected, result);
        }

        public void DeadCellCase4Lives()
        {
            Game game = new Game(3, 3);

            //Inicialización de juego
            game.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(0, 1),
                new KeyValuePair<int, int>(1, 0),
                new KeyValuePair<int, int>(1, 2),
                new KeyValuePair<int, int>(2, 1)
            });

            bool? expected = false;

            game.Play();

            bool? result = game.IsAlive(1, 1);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void DeadCellCase5Lives()
        {
            Game game = new Game(3, 3);

            //Inicialización de juego
            game.Init(new List<KeyValuePair<int, int>>() {
                new KeyValuePair<int, int>(2, 0),
                new KeyValuePair<int, int>(2, 1),
                new KeyValuePair<int, int>(2, 2),
                new KeyValuePair<int, int>(0, 2),
                new KeyValuePair<int, int>(1, 2)
            });

            bool? expected = false;

            game.Play();

            bool? result = game.IsAlive(1, 1);

            Assert.AreEqual(expected, result);
        }
    }
}
